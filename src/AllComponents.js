import React from 'react';
import { Utils, Button, ButtonGroup, NavBar, NavLink, Popup } from 'simple-react-components';
import EventEmitter from 'events';

class AllComponents extends React.Component {
    constructor(props) {
        super(props);

        this._onShowPopup = this._onShowPopup.bind(this);
        this._onHidePopup = this._onHidePopup.bind(this);
        this._onButtonGroupClick = this._onButtonGroupClick.bind(this);
        this._onPopupFadeInComplete = this._onPopupFadeInComplete.bind(this);
        this._onPopupFadeOutComplete = this._onPopupFadeOutComplete.bind(this);

        this.state = {
            popupShowing: false
        };

        this._coinSize = 0;
        window.utils = Utils;
        this._emitter = new EventEmitter();
    }

    render() {
        return (
            <div>
                <div className='nav-buffer' />
                <NavBar>
                    <NavLink href='/'>HOME</NavLink>
                    <NavLink href='/about'>ABOUT</NavLink>
                    <NavLink href='/blog'>BLOG</NavLink>
                    <NavLink href='/something'>SOMETHING</NavLink>
                </NavBar>
                <div className='content'>
                    <Button onTouchTap={this._onShowPopup}
                            label='Show Popup'
                            mods={['primary']}
                    />
                    <Button onTouchTap={this._onHidePopup}
                            label='Hide Popup'
                            mods={['secondary']}
                    />
                    <ButtonGroup onButtonClick={this._onButtonGroupClick}
                                 enabled={false}
                    >
                        <Button key='btn1' label='Button1' />
                        <Button key='btn2' label='Button2' />
                        <Button key='btn3' label='Button3' />
                    </ButtonGroup>
                    {this.state.popupShowing ? this._getPopup() : null}
                </div>
            </div>
        );
    }

    _getPopup() {
        const WrappedPopup = this._wrapPopup(Popup, this._emitter);

        return (
            <WrappedPopup fadeOutAnimClass='popup-disappear'
                          fadeOutEndClass='popup-end'
                          onFadeOutComplete={this._onPopupFadeOutComplete}
                          autoFadeOutDelayMS={1000}
                          titleText='Value: '
                          coinSize={this._coinSize}>
            </WrappedPopup>
        )
    }

    _wrapPopup(PopupComponent, emitter) {
        return class extends React.Component {
            constructor(props) {
                super(props);
                this._emitter = emitter;

                this._onCoinSizeChange = this._onCoinSizeChange.bind(this);
                this.state = {
                    coinSize: this.props.coinSize
                };
            }

            componentDidMount() {
                this._emitter.addListener('change', this._onCoinSizeChange);
            }

            componentWillUnmount() {
                this._emitter.removeListener('change', this._onCoinSizeChange);
            }

            _onCoinSizeChange(value) {
                this.setState({
                    coinSize: value
                });
            }

            render() {
                return <PopupComponent contentText={this.state.coinSize} {...this.props} />;
            }
        }
    }

    _onPopupFadeInComplete() {
        console.log('>>>>>>>>[AllComponents] _onPopupFadeInComplete() - ');
    }

    _onPopupFadeOutComplete() {
        console.log('>>>>>>>>[AllComponents] _onPopupFadeOutComplete() - ');
        this._onHidePopup();
    }

    _onShowPopup() {
        this.setState({
            popupShowing: true
        });
    }

    _onHidePopup() {
        this.setState({
            popupShowing: false
        });
    }

    _onButtonGroupClick(index) {
        console.log('>>>>>>>>[AllComponents] _onButtonGroupClick() - index: ', index);
        if (index === 0) {
            this._emitter.emit('change', --this._coinSize);
        }

        if (index === 1) {
            this._emitter.emit('change', ++this._coinSize);
        }
    }
}

export default AllComponents;
