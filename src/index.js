import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();
import AllComponents from './AllComponents';

ReactDOM.render(<AllComponents />, document.getElementById('root'));
