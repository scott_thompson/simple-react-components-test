import babel from 'rollup-plugin-babel';
import nodeResolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import builtins from 'rollup-plugin-node-builtins';
// import replace from 'rollup-plugin-replace';

export default {
    input: 'src/index.js',
    acorn: {allowReserved: true},
    plugins: [
        builtins(),
        nodeResolve({
            module: true,
            main: true
        }),
        commonjs({
            include: 'node_modules/**',
            sourceMap: false
        }),
        babel({
            babelrc: true
            // exclude: 'node_modules/**'
        })
        // replace({
        //     'process.env.NODE_ENV': JSON.stringify('production')
        // })
    ],
    // external: ['react', 'react-dom'],
    output: {
        file: 'output/index.js',
        format: 'umd'
    }
};
